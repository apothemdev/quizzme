<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd" />

<!--Noops!-->

<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
    <head>
        <title>Quizzme</title>
        <meta charset="UTF-8" />
        <meta name="apple-mobile-web-app-capable" content="yes" />
        <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent" />
        <link rel="stylesheet" type="text/css" href="/css/global.css" />
        <link rel="icon" type="image/x-icon" href="/media/favicon/favicon-standard.ico" />
        <link rel="shortcut icon" type="image/x-icon" href="/media/favicon/favicon-ie9-pin.ico" />
        <link rel="apple-touch-icon" type="image/x-icon" sizes="54x54" href="/media/favicon/favicon-ipad-mini.ico" />
        <link rel="apple-touch-icon" type="image/x-icon" sizes="57x57" href="/media/favicon/favicon-ipod-touch.ico" />
        <link rel="apple-touch-icon" type="image/x-icon" sizes="72x72" href="/media/favicon/favicon-ipad.ico" />
        <link rel="apple-touch-icon" type="image/x-icon" sizes="114x114" href="/media/favicon/favicon-iphone-generation-4.ico" />
        <link rel="stylesheet" type="text/css" href="/css/global.css" />
    </head>
    <body>
        <h1>Quizzme<sup>alpha</sup></h1>

        <div id="nav">//Navigation menu element for script file</div>
        <script src="/jscript/navMenu.js"></script>

        <h2>Register</h2>

        <?php
            $status = $_GET["pass"];

            if($status === "email") {
                echo("<p class=\"error\">Email address already taken.</p>");
            }

            if($status === "password") {
                echo("<p class=\"error\">Passwords entered did not match.</p>");
            }
        ?>

        <form method="POST" action="/cgi-bin/createAccount.php">
            <legend>Register for Quizzme</legend>

            <label>Email</label>
            <input name="EMAIL" type="text" /><br />

            <label>Password</label>
            <input name="PASSWORD" type="password" /><br />

            <label>Confirm</label>
            <input name="CONFIRM" type="password" /><br />

            <label>Name</label>
            <input name="NAME" type="text" /><br />

            <input name="SUBMIT" type="submit" value="Register" />
        </form>

        <p><b>Found a bug? Report it! Head on over to
            <a href="https://bitbucket.org/apothemdev/quizzme/issues/new">
            our issue tracker</a>!</b></p>
        <p>SLInsight Site Integrity Score: <img src="https://insight.sensiolabs.com/projects/80c24d18-c85b-4628-9cf5-700e8d692359/small.png" /></p>
    </body>
</html>
