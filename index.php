<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd" />

<!--Watch out for that Walker!-->

<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
    <head>
        <title>Quizzme</title>
        <meta charset="UTF-8" />
        <meta name="apple-mobile-web-app-capable" content="yes" />
        <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent" />
        <link rel="stylesheet" type="text/css" href="/css/global.css" />
        <link rel="icon" type="image/x-icon" href="/media/favicon/favicon-standard.ico" />
        <link rel="shortcut icon" type="image/x-icon" href="/media/favicon/favicon-ie9-pin.ico" />
        <link rel="apple-touch-icon" type="image/x-icon" sizes="54x54" href="/media/favicon/favicon-ipad-mini.ico" />
        <link rel="apple-touch-icon" type="image/x-icon" sizes="57x57" href="/media/favicon/favicon-ipod-touch.ico" />
        <link rel="apple-touch-icon" type="image/x-icon" sizes="72x72" href="/media/favicon/favicon-ipad.ico" />
        <link rel="apple-touch-icon" type="image/x-icon" sizes="114x114" href="/media/favicon/favicon-iphone-generation-4.ico" />
        <link rel="stylesheet" type="text/css" href="/css/global.css" />
    </head>
    <body>
        <h1>Quizzme<sup>alpha</sup></h1>

        <b>
        <?php
            if(isset($_COOKIE['user_name'])) {
                echo("Hello, " . $_COOKIE['user_name']);
                echo("<br />");
                echo("<a href=\"/cgi-bin/endSession.php\">Logout</a>");
            } else {
                echo("<a href=\"/login.php\">Login or Register</a>");
            }
        ?>
        </b>

        <div id="nav">//Navigation menu element for script file</div>
        <script language="Javascript" src="/jscript/navMenu.js"></script>

        <h2>Welcome</h2>
        <p>Welcome to Quizzme. Quizzme is a work-in-progress web-based application
        allowing nearly anyone to set up and host their very own trivia event.
        With Quizzme, unlike other popular services, you are able to host trivia
        tournaments, as well as classic indvidual events.</p>
        <p><i>More information will come as development matures.</i></p>
        <p><b>Found a bug? Report it! Head on over to
            <a href="https://bitbucket.org/apothemdev/quizzme/issues/new">
            our issue tracker</a>!</b></p>
        <p>SLInsight Site Integrity Score: <img src="https://insight.sensiolabs.com/projects/80c24d18-c85b-4628-9cf5-700e8d692359/small.png" /></p>
    </body>
</html>
